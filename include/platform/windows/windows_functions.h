#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
#ifndef WINDOWS_FUNCTIONS_H
#define WINDOWS_FUNCTIONS_H
#include <Windows.h>
void *FREELOID_LoadLibrary(const char *filePath){
    return LoadLibraryA(filePath);
}
#endif
#endif