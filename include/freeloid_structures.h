#ifndef FREELOID_STRUCTURES
#define FREELOID_STRUCTURES
typedef struct
{
    const char *Name;
    const void *Handle;
} FREELOID_FUNCTION;

typedef struct
{
    void* Handle;
    const char* Name;
    int functionsCount;
    FREELOID_FUNCTION *Functions;
} FREELOID_LIBRARY;

typedef struct
{
    FREELOID_LIBRARY OpenVS4; // Piapro initializer
    FREELOID_LIBRARY vedit; // VOCALOID4 Editor initializer
    FREELOID_LIBRARY vsq4;
    FREELOID_LIBRARY va_DBM4;
    FREELOID_LIBRARY va_DSE4;
    FREELOID_LIBRARY va_DSE4_DFT;
    FREELOID_LIBRARY va_g2pa4_CHS;
    FREELOID_LIBRARY va_g2pa4_ENG;
    FREELOID_LIBRARY va_g2pa4_ESP;
    FREELOID_LIBRARY va_g2pa4_JPN;
    FREELOID_LIBRARY va_g2pa4_KOR;
    FREELOID_LIBRARY va_udm4_ENG;
} FREELOID_VOCALOID4_LIBRARIES;

typedef struct{
    FREELOID_LIBRARY vedit3; // VOCALOID3 Editor initializer
    FREELOID_LIBRARY vsq3;
    FREELOID_LIBRARY dbm3;
    FREELOID_LIBRARY DSE3;
    FREELOID_LIBRARY DSE3_DFT;
    FREELOID_LIBRARY va_g2pa3_CHS;
    FREELOID_LIBRARY va_g2pa3_ENG;
    FREELOID_LIBRARY va_g2pa3_ESP;
    FREELOID_LIBRARY va_g2pa3_KOR;
    FREELOID_LIBRARY va_g2pa3_JPN;
    FREELOID_LIBRARY udm3_ENG;
} FREELOID_VOCALOID3_LIBRARIES;

#endif